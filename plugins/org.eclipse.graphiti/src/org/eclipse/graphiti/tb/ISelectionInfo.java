/*********************************************************************
* Copyright (c) 2005, 2024 SAP SE and others
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mgorning - Bug 391523 - Revise getSelectionInfo...() in IToolBehaviorProvider
*    gergo - Gitlab issue 5 - Line width of the selection rectangle cannot be set
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.tb;

import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.util.IColorConstant;

/**
 * The Interface ISelectionInfo.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface ISelectionInfo {

	/**
	 * Gets the color.
	 * 
	 * @return the color of the selection border
	 */
	IColorConstant getColor();

	/**
	 * Gets the hover color.
	 * 
	 * @return the on hover color
	 */
	IColorConstant getHoverColor();

	/**
	 * Gets the hover color for a shape whose parent is selected.
	 * 
	 * @return the hover color
	 */
	IColorConstant getHoverColorParentSelected();

	/**
	 * Gets the line style.
	 * 
	 * @return the line style of the selection
	 */
	LineStyle getLineStyle();

	/**
	 * Gets the line width.
	 * 
	 * @return the line width of the selection
	 * @since 0.19
	 */
	int getLineWidth();

	/**
	 * Set the color of the selection.
	 * 
	 * @param color
	 *            the color
	 */
	void setColor(IColorConstant color);

	/**
	 * Sets the hover color.
	 * 
	 * @param hoverColor
	 *            the color
	 */
	void setHoverColor(IColorConstant hoverColor);

	/**
	 * Sets the hover color for shapes whose parent is selected.
	 * 
	 * @param hoverColor
	 *            the color
	 */
	void setHoverColorParentSelected(IColorConstant hoverColor);

	/**
	 * Set the line style of the selection.
	 * 
	 * @param lineStyle
	 *            the line style
	 */
	void setLineStyle(LineStyle lineStyle);

	/**
	 * Sets the line width of the selection.
	 * 
	 * @param lineWidth
	 *            the line width
	 * @since 0.19
	 */
	void setLineWidth(int lineWidth);

}
