/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.util;

import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.Shape;

/**
 * The Class LocationInfo.
 */
public class LocationInfo implements ILocationInfo {

	private Shape shape;

	private GraphicsAlgorithm graphicsAlgorithm;

	/**
	 * Creates a new {@link LocationInfo} with given shape and graphics
	 * algorithm.
	 * 
	 * @param shape
	 *            the shape
	 * @param graphicsAlgorithm
	 *            the graphics algorithm
	 */
	public LocationInfo(Shape shape, GraphicsAlgorithm graphicsAlgorithm) {
		this.shape = shape;
		this.graphicsAlgorithm = graphicsAlgorithm;
	}

	public Shape getShape() {
		return this.shape;
	}

	public GraphicsAlgorithm getGraphicsAlgorithm() {
		return this.graphicsAlgorithm;
	}
}
