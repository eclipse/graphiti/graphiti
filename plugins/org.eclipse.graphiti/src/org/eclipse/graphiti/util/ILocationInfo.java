/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.util;

import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.Shape;

/**
 * The Interface ILocationInfo.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface ILocationInfo {

	/**
	 * Gets the shape.
	 * 
	 * @return the shape of this location information
	 */
	Shape getShape();

	/**
	 * Gets the graphics algorithm.
	 * 
	 * @return the graphics algorithm of this location information
	 */
	GraphicsAlgorithm getGraphicsAlgorithm();
}
