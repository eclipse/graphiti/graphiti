/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE and others
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    Patch 185019 from Bug 332360 contributed by Volker Wegert
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features;

import org.eclipse.graphiti.features.context.IResizeShapeContext;

/**
 * The Interface IResizeFeature.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface IResizeFeature extends IFeature {

	/**
	 * Provides configuration object, which describes the resize behavior
	 * 
	 * @param context the resizing context
	 * @return configuration object
	 */
	IResizeConfiguration getResizeConfiguration(IResizeShapeContext context);
}
