/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context.impl;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.internal.features.context.impl.base.PictogramElementContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * The Class LayoutContext.
 */
public class LayoutContext extends PictogramElementContext implements ILayoutContext {

	/**
	 * Creates a new {@link LayoutContext}.
	 * 
	 * @param pictogramElement
	 *            the pictogram element
	 */
	public LayoutContext(PictogramElement pictogramElement) {
		super(pictogramElement);
	}
}