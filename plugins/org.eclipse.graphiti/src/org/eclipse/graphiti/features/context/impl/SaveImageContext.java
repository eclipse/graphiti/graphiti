/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 323155 - Check usage scenarios for DefaultPrintFeature and
*            DefaultSaveImageFeature
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context.impl;

import org.eclipse.graphiti.features.context.ISaveImageContext;
import org.eclipse.graphiti.internal.features.context.impl.base.DefaultContext;

/**
 * The Class SaveImageContext.
 */
public class SaveImageContext extends DefaultContext implements ISaveImageContext {

	/**
	 * Creates a new {@link SaveImageContext}.
	 */
	public SaveImageContext() {
		super();
	}
}
