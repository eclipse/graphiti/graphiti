/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context.impl;

import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;

/**
 * The Class AddConnectionContext.
 */
public class AddConnectionContext extends AddContext implements IAddConnectionContext {

	/**
	 * The target anchor.
	 */
	Anchor sourceAnchor, targetAnchor;

	/**
	 * Creates a new {@link AddConnectionContext}.
	 * 
	 * @param sourceAnchor
	 *            the source anchor
	 * @param targetAnchor
	 *            the target anchor
	 */
	public AddConnectionContext(Anchor sourceAnchor, Anchor targetAnchor) {
		super();
		this.sourceAnchor = sourceAnchor;
		this.targetAnchor = targetAnchor;
	}

	public Anchor getSourceAnchor() {
		return this.sourceAnchor;
	}

	public Anchor getTargetAnchor() {
		return this.targetAnchor;
	}

	@Override
	public String toString() {
		String ret = super.toString();
		return ret + " sourceAnchore: " + getSourceAnchor() + " targetAnchor: " + getTargetAnchor(); //$NON-NLS-1$ //$NON-NLS-2$
	}

}
