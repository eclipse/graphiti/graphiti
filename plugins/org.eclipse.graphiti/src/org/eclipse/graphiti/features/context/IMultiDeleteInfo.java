/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context;

/**
 * The Interface IMultiDeleteInfo.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 * @since 0.8
 */
public interface IMultiDeleteInfo {

	/**
	 * Returns whether a dialog can be shown or should be suppressed.
	 * 
	 * @return TRUE, a dialog can be shown
	 */
	boolean isShowDialog();

	/**
	 * Sets whether a dialog can be shown or should be suppressed.
	 * 
	 * @param showDialog
	 *            TRUE, a dialog can be shown
	 */
	void setShowDialog(boolean showDialog);

	/**
	 * Returns the state whether delete has been canceled or not.
	 * 
	 * @return TRUE if delete canceled; FALSE in all other cases
	 */
	boolean isDeleteCanceled();

	/**
	 * Sets the state whether delete has been canceled or not.
	 * 
	 * @param deleteCanceled
	 *            TRUE, if delete canceled; FALSE in all other cases
	 */
	void setDeleteCanceled(boolean deleteCanceled);

	/**
	 * Returns the number of elements which are selected for deletion.
	 * 
	 * @return the number of selected elements
	 */
	int getNumber();
}
