/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE and others
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    Patch 185019 from Bug 332360 contributed by Volker Wegert
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features;

/**
 * 
 * @noextend This interface is not intended to be extended by clients.
 * @noimplement This interface is not intended to be implemented by clients,
 *              extend {@link DefaultResizeConfiguration} instead.
 */
public interface IResizeConfiguration {
	/**
	 * Defines resize behavior
	 * 
	 * @return true if element can be resized in vertical direction
	 */
	boolean isVerticalResizeAllowed();

	/**
	 * Defines resize behavior
	 * 
	 * @return true if element can be resized in horizontal direction
	 */
	boolean isHorizontalResizeAllowed();
}
