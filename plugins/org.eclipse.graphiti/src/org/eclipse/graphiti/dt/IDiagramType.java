/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.dt;

import org.eclipse.graphiti.platform.IExtension;

/**
 * The Interface IDiagramType.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface IDiagramType extends IExtension {

	/**
	 * Gets the id.
	 * 
	 * @return the id of the new diagram type
	 */
	String getId();

	/**
	 * Gets the name.
	 * 
	 * @return the name of the new diagram type
	 */
	String getName();

	/**
	 * Gets the description.
	 * 
	 * @return the description of the new diagram type
	 */
	String getDescription();
}
