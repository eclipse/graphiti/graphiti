/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.internal.command;

import org.eclipse.graphiti.IDescription;
import org.eclipse.graphiti.features.IFeatureProviderHolder;

/**
 * The Interface ICommand. Defines the general GF Command
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public interface ICommand extends IDescription, IFeatureProviderHolder {

	/**
	 * Can execute.
	 * 
	 * @return true, if successful
	 */
	boolean canExecute();

	/**
	 * Execute.
	 * 
	 * @return true, if successful
	 */
	boolean execute();

	/**
	 * Can undo.
	 * 
	 * @return true, if successful
	 */
	boolean canUndo();

	/**
	 * Undo.
	 * 
	 * @return true, if successful
	 */
	boolean undo();
}
