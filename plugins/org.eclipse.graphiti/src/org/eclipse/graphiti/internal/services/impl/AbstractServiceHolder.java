/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.internal.services.impl;

import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeService;

/**
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
abstract class AbstractServiceHolder {

	private IGaService gaService;
	private IPeService peService;

	public AbstractServiceHolder() {
		super();
	}

	protected IGaService getGaService() {
		if (gaService == null) {
			gaService = Graphiti.getGaService();
		}
		return gaService;
	}

	protected IPeService getPeService() {
		if (peService == null) {
			peService = Graphiti.getPeService();
		}
		return peService;
	}
}