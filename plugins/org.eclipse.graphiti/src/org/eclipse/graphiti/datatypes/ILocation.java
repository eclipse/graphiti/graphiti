/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.datatypes;

/**
 * The Interface ILocation.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface ILocation {

	/**
	 * Gets the y value.
	 * 
	 * @return the y value of this location
	 */
	int getY();

	/**
	 * Sets the y coordinate of this location.
	 * 
	 * @param y
	 *            the new y coordinate
	 */
	void setY(int y);

	/**
	 * Gets the x value.
	 * 
	 * @return the x value of this location
	 */
	int getX();

	/**
	 * Sets the x coordinate of this location.
	 * 
	 * @param x
	 *            the new x coordinate
	 */
	void setX(int x);
}
