/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 325084 - Provide documentation for Patterns
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.pattern;

/**
 * The Interface IFeatureForPattern marks a feature as being executable inside a
 * pattern. It is used for all the features that wrap pattern functionality and
 * should not be used by clients directly.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients. Extend
 *           {@link FeatureForPatternDelegate} instead
 */
public interface IFeatureForPattern {

	/**
	 * Gets the pattern.
	 * 
	 * @return the pattern
	 */
	IPattern getPattern();
}
