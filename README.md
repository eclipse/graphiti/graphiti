# Graphiti

This is the main Git repository for [Graphiti](https://eclipse.dev/graphiti).

- https://eclipse.dev/graphiti/
- https://projects.eclipse.org/projects/modeling.gmp.graphiti

## Updates

Updates are available here:

- https://download.eclipse.org/graphiti/updates/

## Builds

Builds are produced here:

- https://ci.eclipse.org/graphiti

## Contributing

Contributions are welcome:

- [CONTRIBUTING.md](CONTRIBUTING.md)

