/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 352709 - invalid image provider id crashes diagram editor 
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.bot.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

public class GFTestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite(GFTestSuite.class.getName());
		suite.addTestSuite(GFOtherTests.class);
		suite.addTestSuite(GFPackageTests.class);
		suite.addTestSuite(GFDialogTests.class);
		suite.addTestSuite(GFInteractionComponentTests.class);
		suite.addTestSuite(ExtensionManagerTest.class);
		suite.addTestSuite(PluginWizardTests.class);
		return suite;
	}
}
