/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.testtool.sketch;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.graphiti.platform.ga.IGraphicsAlgorithmRenderer;
import org.eclipse.graphiti.platform.ga.IGraphicsAlgorithmRendererFactory;
import org.eclipse.graphiti.platform.ga.IRendererContext;

/**
 * A factory for creating SketchGraphicsAlgorithmRenderer objects.
 */
public class SketchGraphicsAlgorithmRendererFactory implements IGraphicsAlgorithmRendererFactory {

	/**
	 * The Constant CANFIGURE.
	 */
	public final static String CANFIGURE = "sketch.canfigure";

	public IGraphicsAlgorithmRenderer createGraphicsAlgorithmRenderer(IRendererContext context) {
		if (CANFIGURE.equals(context.getPlatformGraphicsAlgorithm().getId())) {
			return createCanFigure();
		}
		return null;
	}

	private CanFigure createCanFigure() {
		CanFigure ret = new CanFigure(false);
		ret.setDropShadow(false);
		ret.setTextColor(ColorConstants.blue);
		ret.setBackgroundColor(ColorConstants.yellow);
		ret.setCaption("can figure test");
		return ret;

	}
}
