/*********************************************************************
* Copyright (c) 2011, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.examples.mm.chess;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Board</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link org.eclipse.graphiti.examples.mm.chess.Board#getSquares
 * <em>Squares</em>}</li>
 * <li>{@link org.eclipse.graphiti.examples.mm.chess.Board#getPieces
 * <em>Pieces</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.graphiti.examples.mm.chess.ChessPackage#getBoard()
 * @model
 * @generated
 */
public interface Board extends EObject {
	/**
	 * Returns the value of the '<em><b>Squares</b></em>' containment reference
	 * list. The list contents are of type
	 * {@link org.eclipse.graphiti.examples.mm.chess.Square}. It is
	 * bidirectional and its opposite is
	 * '{@link org.eclipse.graphiti.examples.mm.chess.Square#getBoard
	 * <em>Board</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Squares</em>' containment reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Squares</em>' containment reference list.
	 * @see org.eclipse.graphiti.examples.mm.chess.ChessPackage#getBoard_Squares()
	 * @see org.eclipse.graphiti.examples.mm.chess.Square#getBoard
	 * @model opposite="board" containment="true" lower="64" upper="64"
	 * @generated
	 */
	EList<Square> getSquares();

	/**
	 * Returns the value of the '<em><b>Pieces</b></em>' containment reference
	 * list. The list contents are of type
	 * {@link org.eclipse.graphiti.examples.mm.chess.Piece}. It is bidirectional
	 * and its opposite is
	 * '{@link org.eclipse.graphiti.examples.mm.chess.Piece#getBoard
	 * <em>Board</em>}'. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pieces</em>' containment reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Pieces</em>' containment reference list.
	 * @see org.eclipse.graphiti.examples.mm.chess.ChessPackage#getBoard_Pieces()
	 * @see org.eclipse.graphiti.examples.mm.chess.Piece#getBoard
	 * @model opposite="board" containment="true" upper="32"
	 * @generated
	 */
	EList<Piece> getPieces();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @model required="true" rankRequired="true" fileRequired="true"
	 * @generated
	 */
	Square getSquare(Ranks rank, Files file);

} // Board
