/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.examples.common.navigator.nodes.base;

import org.eclipse.core.resources.IProject;

/**
 * The Class AbstractInstancesOfTypeContainerNode.
 */
public abstract class AbstractInstancesOfTypeContainerNode extends AbstractContainerNode {

	private Object parent;
	IProject project;

	/**
	 * The Constructor.
	 * 
	 * @param parent
	 *            the parent
	 */
	public AbstractInstancesOfTypeContainerNode(Object parent, IProject project) {
		super();
		this.parent = parent;
		this.project = project;
	}

	public Object getParent() {
		return parent;
	}

	@Override
	public boolean hasChildren() {
		return super.hasChildren(); // getChildren().length > 0;
	}

	public IProject getProject() {
		return project;
	}
}
