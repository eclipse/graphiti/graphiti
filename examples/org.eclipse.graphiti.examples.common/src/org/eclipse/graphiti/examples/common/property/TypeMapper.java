/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.examples.common.property;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.ui.views.properties.tabbed.ITypeMapper;

public class TypeMapper implements ITypeMapper {

	public Class<? extends Object> mapType(Object object) {

		Class<? extends Object> type = object.getClass();
		if (object instanceof EditPart) {

			Object model = ((EditPart) object).getModel();

			type = model.getClass();
			if (model instanceof PictogramElement) {
				PictogramElement pe = (PictogramElement) model;

				EObject businessObject = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
				if (businessObject == null) {
					return pe.getClass();
				} else {
					return businessObject.eClass().getClass();
				}
			}

		}
		return type;
	}
}